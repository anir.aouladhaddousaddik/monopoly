# Title page

* assignment : Data Analysis
* game : Monopoly Terminal Game
* group : Lecturer

# Domain Model

## Step 1 - gather all candidate nouns from the input documents 

### input : Assignment details

... create a **MonopolyApplication** class ... it has a main class

... keep track of **score**s of multiple **game**s,

... after playing 2 **round**s, and halfway through the third round of a game of 3 **player**s.

... did you keep track of all previous **action**s in all the **turn**s.

### input : Project description

... A simple **board** is drawn to show the status of the game

... **name**s of all players, the

... One by One the **player**s take **turn**s.

... turn with a roll of the **dice**.

... of the **die**, their **pawn** is moved to a new **position**

... on, different **action**s are provided to the user

... o Buy a **Property**, Build **Housing** or other automated actions to pay
**rent** or get **money** from the **bank**.

... Players should be able to see all **propertie**s and **money** of all **player**s

... the **score** is then calculated based on the **amount** of money & the **value** of properties.

... Those **score**s are to be saved with at minimum the following information : **name of player**,
a full **trace** of all actions/turns/rounds of that game, the **total wealth** as score, the **duration** of the game

... there should be a **leaderboard** and it should function as follows: you keep track of the **player’s names**, the **date**, and the **time**.

### input : Slides

... players moving around the **square**s of the board

... _Two to eight_ **player**s can play

... A **game** is played as a series of **round**s. During a **round**, each player
takes one **turn**. In each turn, a player advances his **piece** clockwise
around the **board** a number of **square**s equal to the sum of the
number rolled on two six-sided **dice**

... The **square name**s will be “Go”, “Square 1”, “Square 2“

... There are “Lots”, “Railroads” and “Utility” **square**s

... on the square must pay its **owner** rent


## Step 2A - convert the nouns into  class/attribute or association   

| Noun                                        | Type                   | comment                                                                                                                                                                            |
|:--------------------------------------------|:-----------------------|:-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| **MonopolyApplication**                     | class                  | has a method called main()                                                                                                                                                         |
| **score**                                   | class or attribute     | score of a game ->  attribute ?                                                                                                                                                    |
| **game**                                    | class                  | multiple games, so which class is associated to all these games?                                                                                                                   |
| **round**                                   | class or attribute     | just a number? -> attribute, could be associated with other stuff -> class                                                                                                         |
| **player**                                  | class                  |                                                                                                                                                                                    |
| **action**s                                 | class or attribute     | just a text? -> attribute or is it more than that?                                                                                                                                 |
| **turn**                                    | class or attribute     | just a number? -> attribute, or is it more than that?                                                                                                                              |
| **board**                                   | class                  | cannot be respresented by a primitive datatype (int, float,string, date, .)                                                                                                        |
| **name** of player                          | attribute              | -> player.name : string                                                                                                                                                            |
| players take **turn**s                      | class                  | either it's an attibute of a player - but it changes all the time? it is/has a number and belongs to a player -> so it should be associated with a player. Hence **turn=class**    |
| **dice**                                    | class ~~or attribute~~ | it has a facevalue, we have 2 of them, but roll of the dice is a potential action (thus. method) -> class seems more suited. Singular -> **die=class**                             |
| **pawn**                                    | class                  | it belongs to a player, sits on the board and can be moved, it probably has could also have a colour attribute                                                                     |
| **position**                                | ~~class or~~ attribute | a number between 0 and 40 and attribute of the pawn                                                                                                                                |
| **action**                                  | class or attribute     | either a string values like _roll, buy, rent, ..._  or maybe it has itself properties .. let's wait and see                                                                        |
| **property**                                | class                  | it has an attribute type of the squares on which you can land. it can be owned by a player. it has street name and a predefined (enumeration) color                                | 
| **housing**                                 | ~~class or~~ attribute | it could be represented as an attribute of a property (0, 1,2,3,4 or "hotel") or could be a class with a color attribute, but that seems overkill.                                 |
| **rent**                                    | attribute              | it is an attribute which is related to a property and defined by the number of houses on the property                                                                              |
| **money**                                   | attribute              | an attribute from the player or the bank                                                                                                                                           |
| **bank**                                    |                        | could be a class, but in reality the computer system will act a bank, so no separate class needed                                                                                  | 
| **score**                                   | (calculated) attribute | attribute of a player which is calculated from the money and value of properties                                                                                                   |
| **amount**                                  | _synonym_              | synonym for  attribute 'money'                                                                                                                                                     |
| **score**                                   | class                  | it has properties itself see below                                                                                                                                                 |
| **name of player**                          | attribute              | also a (calculated) attribute for score                                                                                                                                            | 
| **trace**                                   | attribute?             | it is not clear, but can be a string calculated **actions/turns/rounds** of that player in the game. So these should all be classes?                                               | 
| **total wealth**                            | attribute              | score was an attribute, next we decided it to be a class, so **wealth** is float calculated from money and properties                                                              | 
| **duration**                                | attribute              | an attribute of the game played by this player. As a consequence, we should have a start and stop time for each turn? Which allows to calculate duration of whole game             |
| **leaderboard**                             | report?                | if we keep track of all **score**s, than we could generate this leaderboard.                                                                                                       |
| **date** and **time**                       | attribute              | an attribute of the game started? ended by this player. If it's an attribute of score - we can use end time                                                                        |
| **square**                                  | class                  | there are 40 squares on the board. a square can be a Property or Start or .. as an identification we could use the attribute 'position'. Previously we used pawn position          |
| **piece**                                   | _synonym_              | same as class Pawn                                                                                                                                                                 | 
| **dice**                                    | class                  | previously we defined Die as a class, a Die can be rolled. But actually we always roll TWO dice at once. Let's put them in a **Cup** to roll two dice together                     |
| **square name**                             | attibute               | an extra attribute for the square, square @ position 0 will be named "Go", the one aty position 10 can be called "Jail"                                                            |  
| **Lot** <br/> **Railroad**<br/> **Utility** | attribute or class     | either these are the values of an attribute **type** of a square, or it could be specialisations of a square with each having different properties/behaviour .. let's wait and see |
| **owner**  or  _is owned by_                | association            | commercial rule : Every Player can own 0 to many Square(s), Every square can be owned by 0 .. 1 Player                                                                             |

  

## Step 2B - consolidate classes and attributes     

``` plantuml
@startuml
skinparam style strictuml

class MonopolyApplication{
  main()
}

Class Score{
  amountOfMoney:float
  valueOfOwnedProperties : float[]
  /totalWealth:float
  playerName:String
  trace:String[]
  turnDurations : float[]
  endDate:Date
}
                                   
Class Game{
  startDate
  endDate
}                                    

Class Round{}
                                   
Class Player{
 name
 money
}
                               
Class Action{}
                                 
Class Turn{
  startDateTime
  endDateTime
  /duration 
}                                    

Class Board{}
                      
' 2 Dice in a Cup 
Class Cup{}                                    
Class Die{}                                    

Class Pawn{
 /position
 color
}                                    
                      
Class Property{
 color:StreetColor 
 housing:Housing
 /rent
 price
}

enum StreetColor{
  Green
  Dark Blue
  Light Blue
  Red Set
  Yellow
  Orange
  Brown
  Purple
}
enum Housing{
  0: "No housing"
  1: "1 House"
  2: "2 Houses"
  3: "3 Houses"
  4: "4 Houses"
  5: "Hotel"
}

' Generalisation of a property
Class Square{
 name
 position
}

' specialisation of Property
Class Lot{}
Class Railroad{}
Class Utility{}

' other special(isation) Square
Class Go{}
Class IncomeTax{}


@enduml
```

## Step 3 - adding the associations + multiplicities 

@todo :
Note : Instead of the MonopolyApplication launching multiple games, 
it is also possible to have a Single Game which is played over multiple GameSessions.


``` plantuml
@startuml
skinparam style strictuml

MonopolyApplication "1" -d- "*" Game : plays >
Game "1" -l- "1" Cup : played with >
Cup "1" -l- "2" Die : holds > 
Game "1" -r- "1" Board : played on >
Game "1" -u- "*" Round
Round "1" -l- "0..8" Turn
Turn "1" -l- "1..*" Action 
Board "1" -d- "40" Square : consists of >
Player "2..8" -u- Game : plays >
Player "1" -d- "1" Score : scores >
Player "1" -r- "1" Pawn : plays with >
Pawn "0..8" -r- "1" Square: landed on >
Player "0..1" -d- "*" Property : owns >
Lot -u-|> Property
Railroad -u-|> Property
Utility -u-|> Property
Property -u-|> Square

Go -u-|> Square
IncomeTax -u-|> Square
@enduml
```

## Step 4A - Bring it all together  

``` plantuml
@startuml
skinparam style strictuml


class MonopolyApplication{
  main()
}

Class Score{
  amountOfMoney:float
  valueOfOwnedProperties : float[]
  /totalWealth:float
  playerName:String
  trace:String[]
  turnDurations : float[]
  endDate:Date
}
                                   
Class Game{
  startDate
  endDate
}                                    

Class Round{}
                                   
Class Player{
 name
 money
}
                               
Class Action{}
                                 
Class Turn{
  startDateTime
  endDateTime
  /duration 
}                                    

Class Board{}
                      
' 2 Dice in a Cup 
Class Cup{}
                               

Class Pawn{
 /position
}                                    
                      
Class Property{
 color:StreetColor 
 housing:Housing
 /rent
 price
}

enum StreetColor{
  Green
  Dark Blue
  Light Blue
  Red Set
  Yellow
  Orange
  Brown
  Purple
}
enum Housing{
  0: "No housing"
  1: "1 House"
  2: "2 Houses"
  3: "3 Houses"
  4: "4 Houses"
  5: "Hotel"
}

' Generalisation of a property
Class Square{
 name
 position
}

' specialisation of Property
Class Lot{}
Class Railroad{}
Class Utility{}

' other special(isation) Square
Class Go{}
Class IncomeTax{}

MonopolyApplication "1" -d- "*" Game : plays >
Game "1" -l- "1" Cup : played with >
Cup "1" -l- "2" Die : holds > 
Game "1" -r- "1" Board : played on >
Game "1" -u- "*" Round
Round "1" -l- "0..8" Turn
Turn "1" -l- "1..*" Action 
Board "1" -d- "40" Square : consists of >
Player "2..8" -u- Game : plays >
Player "1" -d- "1" Score : scores >
Player "1" -r- "1" Pawn : plays with >
Pawn "0..8" -r- "1" Square: landed on >
Player "0..1" -d- "*" Property : owns >
Lot -u-|> Property
Railroad -u-|> Property
Utility -u-|> Property
Property -u-|> Square

Go -u-|> Square
IncomeTax -u-|> Square
@enduml
```

## Step 4B - Generate a more basic version  

``` plantuml
@startuml
skinparam style strictuml

Class Score{
  /amountOfMoney:float
  /valueOfOwnedProperties : float[]
  /totalWealth:float
  /playerName:String
  /endDate:Date
}
                                   
Class Game{
  main()
  startDate
  endDate
}                                    

                                   
Class Player{
 name
 money
}                               

Class Board{}
                                       

Class Pawn{
 /position
}                                    
                      
Class Property{
 color:String 
 housing:String
 /rent
 price
}

' Generalisation of a property
Class Square{
 name
 position
}

Game "1" -l- "2" Die : played with > 
Game "1" -r- "1" Board : played on >
Board "1" -d- "40" Square : consists of >
Player "2..8" -u- Game : plays >
Player "1" -d- "1" Score : scores >
Player "1" -r- "1" Pawn : plays with >
Pawn "0..8" -r- "1" Square: landed on >
Player "0..1" -d- "*" Property : owns >
Property -u-|> Square
@enduml
```


# Java Code

#    

