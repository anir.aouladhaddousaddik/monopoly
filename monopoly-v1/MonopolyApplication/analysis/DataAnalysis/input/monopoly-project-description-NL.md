# Title page

* assignment : Data Analysis
* game : Monopoly Terminal Game
* group : Lecturer

# Project Description


## SPELBESCHRIJVING:
Monopoly is een bordspel waarin meerdere spelers proberen in een immo-markt financiele dominantie (monopolie) proberen verkrijgen. 
Om de beurt zal een speler zijn pion op het bord vooruitbewegen, en op de plaats waar ze landt een reeks acties uitvoeren.
Zo kan ze eigendommen aankopen en daarop een aantal huizen bouwen. 
Wanneer andere spelers dan landen op zo'n eigendom dan zullen ze de eigenaar een huur moeten betalen.
Je wint het spel wanneer alle andere spelers failliet zijn.

Kopie van https://allespelregels.nl/bordspellen/monopoly-spelregels/  volgt hieronder

### Monopoly spelregels
Het doel van het spel is voor iedere speler hetzelfde: een monopolie bemachtigen. Dit doe je door straten te kopen. Je krijgt een bepaald bedrag als een andere speler op je straat komt, de zogenaamde ‘huur’. Als je huizen of hotels bouwt op je straten ontvang je nog meer huur. Als je veel straten, huizen en hotels hebt, stroomt al het geld in het spel naar jou toe. De andere spelers gaan dan failliet en jij wint het spel!

#### 1. Een potje monopoly starten
Om te bepalen wie er mag beginnen gooit iedere spelers de twee dobbelstenen. De speler die het hoogste getal gooit, mag beginnen. Als spelers hetzelfde getal gooien, gooien deze spelers nog een keer. Vervolgens gaat de volgorde met de klok mee.

#### 2. Een beurt spelen
Om een beurt te spelen gooi je de twee dobbelstenen. Het getal dat je gooit, is het aantal stappen dat je mag zetten.

Als je landt op een bezitting (een straat, stations of nutsbedrijf) die nog niet verkocht is, dan kun je deze kopen.

Als je landt op het vakje “naar de gevangenis” moet je dus naar de gevangenis. Dit kan ook gebeuren als je een Algemeen Fonds- of Kanskaart trekt die zegt dat je naar de gevangenis moet, of als je drie keer in één beurt dubbel gooit. Als je naar de gevangenis moet ontvang je geen salaris.

Om de gevangenis uit te komen kun je bijvoorbeeld € 50 betalen en weer verder spelen. Ook kun je de kaart “verlaat de gevangenis zonder te betalen” kopen van een andere speler of gebruiken als je die zelf hebt. Tenslotte kun je drie beurten wachten en tijdens die beurten proberen dubbel te gooien. Na drie beurten in de gevangenis (zonder dat je dubbel gegooid hebt) ben je verplicht € 50 te betalen en de gevangenis te verlaten. Ging het in het echt maar zo makkelijk!

Als je landt op “slechts op bezoek in de gevangenis” hoef je niets te doen. Hetzelfde geldt voor “vrij parkeren”. Als je passeert via “salaris” krijg je € 200. Als je landt op “belasting betalen” moet je, je raadt het al, belasting betalen!

Wanneer je belandt op een bezitting die al verkocht is, dan moet je huur betalen. Hoeveel huur je moet betalen staat op het eigendomsbewijs. De huur wordt hoger dan de speler meer huizen, hotels, stations of nutsbedrijven bezit. Ook dit staat aangegeven op het eigendomsbewijs.

#### 3. Huizen en hotels bouwen
Je mag pas huizen bouwen in een straat als je alle straten can een stad (kleur) bezit. De prijs van en huis vind je ook op het eigendomsbewijs. Je mag pas een tweede huis in een straat bouwen als je op elke straat één huis hebt staan. Je moet dus geleidelijk bouwen. Je mag niet meer bouwen in een stad als er een straat in die stad bezwaard is met een hypotheek.

## PROGRAMMAVEREISTEN
Het uiteindelijke spel zal met volgende eisen rekening moeten houden: 
  * Je zorgt ervoor dat het spel door 2 spelers gespeeld kan worden. Elke speler geeft bij de
start van een nieuw spel zijn naam in. De computer geeft de eerste speler een witte pion, deze speler zal ook het spel starten. De andere speler krijgt de zwarte pion. 
  * Spelers hebben na iedere zet van de vorige speler max. 24u bedenktijd om hun zet uit te voeren, indien dit niet lukt dan wordt de andere speler automatisch de winnaar. 
  * Na afloop van elk spel wordt de uitkomst automatisch opgeslagen, samen met de namen van de spelers, een DateTime stamp wanneer het spel gespeeld is 
   en de uiteindelijke winnaar(of draw). Ook wordt er bijgehouden hoe lang dit spel geduurd heeft, hoeveel ronden er zijn gespeeld, en hoeveel geld iedere speler had op het einde.  
    * Deze lijst met gespeelde spelen kan via het hoofdmenu geraadpleegd worden.
  * De spelregels worden geïmplementeerd zoals hierboven beschreven. Met varianten hoef je nog geen rekening te houden (zie uitbreidingen). 
  * In de eerste fase maak je een console variant, hierbij is het mogelijk een stuk te verplaatsen door de teerlingen te rollen. 

  ## UITBREIDINGEN
Als je een werkend spel hebt geprogrammeerd met een leuke grafische interface, dan kan je
misschien nog een van deze uitbreidingen uitwerken:
* Samen met een tabel om al de afgelopen spelen te raadplegen, is het nu ook mogelijk
een highscores tabel te bekijken waarin staat hoeveel keer elke speler al is gewonnen, en daarnaast ook diens ELO-rating. Zie [hier](https://nl.wikipedia.org/wiki/Elo-rating) om te begrijpen hoe je deze rating berekent.
* Een spel kan op elke moment worden opgeslagen en later hernomen worden.
* Voorzie een single player mode waarbij er tegen de computer gespeeld kan worden. Dit
hoeft geen complexe AI te zijn, maar eerder een algoritme dat rekening houdt met de
spelregels en eventueel enkele strategieën toepast.

