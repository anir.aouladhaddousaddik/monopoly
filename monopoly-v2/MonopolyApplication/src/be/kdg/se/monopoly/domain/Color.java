package be.kdg.se.monopoly.domain;

public enum Color {
    GREEN,
    DARK_BLUE,
    LIGHT_BLUE,
    RED,
    YELLOW,
    ORANGE,
    BROWN,
    PURPLE
}
