package be.kdg.se.monopoly.domain;

import java.util.Random;

public class Die {
    private int faceValue;

    private Random random = new Random();

    public void roll() {
        faceValue = random.nextInt(6);
    }

    public int getFaceValue() {
        return faceValue;
    }
}
