package be.kdg.se.monopoly.domain;

import java.time.Duration;
import java.time.LocalDateTime;
import java.util.ArrayList;

public class Turn {
    //primitive attributes
    private LocalDateTime startDateTime;
    private LocalDateTime endDateTime;

    //conceptual attributes

    private ArrayList<Action> actions; //Turn "1" -l- "1..*" Action


    private Duration getDuration() {
        return Duration.between(startDateTime, endDateTime);
    }


}
